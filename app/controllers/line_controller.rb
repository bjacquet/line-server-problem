# frozen_string_literal: true

class LineController < ApplicationController
  before_action :set_line_index

  attr_reader :line_index

  def get
    server_file = ServerFile.first

    if line_index > server_file.line_count || line_index <= 0
      render plain: 'Line not found', status: 413
    else
      service = FetchLine.new(line_index, server_file)
      line = service.call

      render plain: line, status: 200
    end
  end

  private

  def set_line_index
    @line_index = params[:line_index].to_i
  end
end

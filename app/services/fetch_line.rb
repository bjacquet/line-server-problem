# frozen_string_literal: true

class FetchLine
  attr_reader :requested_line, :server_file

  def initialize(line_index, server_file)
    @requested_line = line_index
    @server_file    = server_file
  end

  def call
    bm_reference = Bookmark
                   .where('line_number <= :line_no', line_no: requested_line)
                   .order(line_number: :desc)
                   .first
    current_line = bm_reference&.line_number || 1
    line_diff    = requested_line - current_line
    file         = File.new(server_file.filename, 'r')

    file.seek(bm_reference&.bytes || 0, IO::SEEK_SET)
    line_diff.times { file.readline }
    file.readline
  end
end

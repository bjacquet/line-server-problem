# frozen_string_literal: true

class ScanFile
  attr_reader :filename, :bytes_per_bookmark

  def initialize(filename, bytes_per_bookmark)
    @filename           = filename
    @bytes_per_bookmark = bytes_per_bookmark
  end

  def call
    ServerFile.transaction do
      server_file        = ServerFile.create(filename: filename)
      accumulative_bytes = 0
      line_no            = 0
      next_save_at       = bytes_per_bookmark

      IO.foreach(server_file.filename) do |line|
        line_no += 1

        if accumulative_bytes >= next_save_at
          server_file.bookmarks.create(
            bytes: accumulative_bytes,
            line_number: line_no
          )
          next_save_at += bytes_per_bookmark
        end

        accumulative_bytes += line.bytesize
      end

      server_file.line_count = line_no
      server_file.save
    end
  end
end

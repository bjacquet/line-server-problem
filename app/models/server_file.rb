# frozen_string_literal: true

class ServerFile < ApplicationRecord
  has_many :bookmarks, dependent: :destroy
end

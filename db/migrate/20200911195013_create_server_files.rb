# frozen_string_literal: true

class CreateServerFiles < ActiveRecord::Migration[6.0]
  def change
    create_table :server_files do |t|
      t.string :filename
      t.bigint :line_count
      t.timestamps
    end
  end
end

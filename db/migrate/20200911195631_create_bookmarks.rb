class CreateBookmarks < ActiveRecord::Migration[6.0]
  def change
    create_table :bookmarks do |t|
      t.belongs_to :server_file, foreign_key: true
      t.bigint     :bytes
      t.bigint     :line_number
      t.timestamps
    end

    add_index :bookmarks, %i[server_file_id line_number]
  end
end

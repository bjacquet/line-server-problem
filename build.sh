#!/bin/bash

set -eu

bundle install
rails db:create
rails db:migrate

echo "Build complete"

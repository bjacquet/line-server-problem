# frozen_string_literal: true

require "rails_helper"

RSpec.describe FetchLine do
  describe '#call' do
    let(:server_file) { ServerFile.first }

    it 'fetches a random page' do
      line_number = rand(1..2748)
      service     = FetchLine.new(line_number, server_file)
      result      = service.call

      expect(result).to eq(fetch_line_aux(line_number, server_file))
    end

    it 'fetches the last page' do
      line_number = 2749
      expected    = "  2749         THE END\n"
      service     = FetchLine.new(line_number, server_file)
      result      = service.call

      expect(result).to eq(expected)
    end
  end

  def fetch_line_aux(line_number, server_file)
    file = File.new(server_file.filename, 'r')

    (line_number - 1).times { file.readline }

    file.readline
  end
end

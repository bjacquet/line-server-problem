# frozen_string_literal: true

require "rails_helper"

RSpec.describe ScanFile do
  describe '#call' do
    let(:filename)           { 'spec/fixtures/example_server_file.txt' }
    let(:bytes_per_bookmark) { 10_000 }

    it 'creates a ServerFile' do
      service = ScanFile.new(filename, bytes_per_bookmark)

      expect { service.call }.to change { ServerFile.count }.by(1)
    end

    it 'creates 12 Bookmarks' do
      service = ScanFile.new(filename, bytes_per_bookmark)

      expect { service.call }.to change { Bookmark.count }.by(12)
    end
  end
end

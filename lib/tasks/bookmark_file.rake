# frozen_string_literal: true

namespace :setup do
  desc 'Parses a file and bookmarks line numbers across the file'
  task :bookmark_file, %i[filename] => [:environment] do |_task, args|
    puts 'Preparing database…'
    ServerFile.destroy_all
    puts 'Done'

    puts 'Scanning the server file…'
    scanner = ScanFile.new(args.filename, ENV.fetch('BYTES_PER_BOOKMARK').to_i)
    scanner.call
    puts 'Scanning complete'
  end
end

# Live Server Problem

## How does your system work?

System setup is performed by running the `build.sh` script

```shell
./build.sh
```

It will download all necessary Ruby Gems; install them; and create a database.

To start the system execute the `run.sh` script as such

```shell
./run.sh filename
```

`filename` is a readable filename path.

The initialization process goes through several steps:

1. Empties all records from the database. When starting the system with a different `filename` it will cleanup leftovers from the previous run.
2. Scans the file, referenced by `filename`, and bookmarks specific line numbers by their byte distance to the beginning of the file.
3. Launches the network server, which awaits for GET requests at http://localhost:3000/line/:line_index
   `:line_index` must be a positive number.

## How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?

The way the system is designed the answer is in two parts.

The bulk of the computation is performed prior to having the system available — part one. This is intended to get shorter response times. Processing a 1.16 GB file takes about 25 seconds — on a 2.3 GHz Dual-Core Intel Core i5. One can assume it will take 10 times more for a 10 GB file.

This computation scans the file line by line. The amount of time needed to scan the file is determined by the file size. Thus _O(n)_, where _n_ represents the size of the file. After every 100000 bytes of lines scanned, the system takes bookmarks the current line in the database. One can vary this number by changing it in the environment variable `BYTES_PER_BOOKMARK`.

When answering to requests — part two — the system consults the database for the closest bookmark to that line. This allow us to begin our seek from that position in the file. It will then jump over line by line until we reach the requested line.

The time to query the database for a bookmark is a _O(1)_ operation. I will assume that positioning a pointer at a specific location in the file to take a linear time consumption. So this will be _O(b)_, where _b_ represents the number of bytes to travel in the file to a bookmark. And last, going from that position to the requested line will also a linear approach _O(l)_, where _l_ represents the amount lines between the bookmark and the requested line.
Putting together we have _O(1 + b + l) = O(b + l)_. This can be reduced to just _O(d)_, where _d_ represents the maximum number of lines that exists between two bookmarks.

## How will your system perform with 100 users? 10000 users? 1000000 users?

This system, the way it's setup, will not perform well with 1000000 requests at the same time. Sorry. It is setup with default parameters which are good enough to handle 100 requests, without significant disruption to the user. And since our service does not use too much resources to provide a response — it does not block the DB for writing; it does not block IO access to the file — it may be good enough for 10000 requests. I didn't measure it. To be able to handle 1000000 requests one would have to consider using multiple machines running the server; add a load balancer; and increase the database connections pool size.

Default DB — Postgresql — parameters mean a connections pool size of 100. The web server — Puma — uses a _threads * workers_ mechanism, defined by environment variables `RAILS_MAX_THREADS` and `WEB_CONCURRENCY` respectively. Regardless of the values we define here, their product can never be higher than the connections pool size.

## What documentation, websites, papers, etc did you consult in doing this assignment?

I could not find any work describing which techniques are best to consult large files. Maybe this sort of challenge is part of a Computer Science field which doesn't terms like _"techniques to read large files effectively"_. When examining Ruby's documentation for the `IO` class and discovering the [`seek`](https://ruby-doc.org/core/IO.html#method-i-seek) method I got the idea to store the "byte position" of lines, and that was it. Until then, I was thinking on splitting the file into chunks and put them in the database. This would probably go against the rules.

## What third-party libraries or other tools does the system use? How did you choose each library or framework you used?

Overall this is a plain Ruby On Rails system. As such, it uses tools like Bundler — to install Gems (libraries); Make — which I use to run tasks prior to run system; and Postgresql — the database. Apart from the Gems required by Rails I installed a couple more to help me with debugging and unit testing.

I choose Rails because it's the framework I'm more familiar with at the moment. Hunchentoot (Common Lisp) or Node (JavaScript) were other options but would have taken me longer — because I would have to remember how to use them.

## How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?

As I write this it as taken me 10h27 to complete the exercise. I would add one hour on top of that for the time spent on it away from the computer — which I did not track. I would also add 30 minutes to finish the remaining answers, prepare the Zip file, and send it back to you.

I see two areas which are left for improvement, both deal with configuration. One is to configure the web server so that it can handle more concurrent requests. This is a completely new challenge to me for I never had to deal with this problem on previous projects. Second is to elaborate a table which would specify the preferable value of `BYTES_PER_BOOKMARK` in accordance to the file size.

I would prioritize them as I referred them: 1) server; 2) bookmarks. I think it's best to have a slow system that can accept requests than to not being able to accept them at all.

## If you were to critique your code, what would you have to say about it?

The `ServerFile` model is a bit unnecessary, if we're only having just one file. The information we use from it could be put on an extra `Bookmark` to represent the last line. However, if we want to add more files that won't need changing.

The selection of the `Bookmark` assumes that we'll continue to read the lines forwards — which we do. Instead, we could see if the _next_ `Bookmark` puts us even closer and if so we would then read the lines backwards.

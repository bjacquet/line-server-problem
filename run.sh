#!/bin/bash

set -eu

RAILS_ENV_FILE=".env"

if [[ $# != 1 ]]; then
    echo "Usage: run.sh filename"
    exit 1
elif [[ ! -r $1 ]]; then
    echo "Unable to read file."
    exit 1
fi

rails setup:bookmark_file"[$1]"

rails s

# frozen_string_literal: true

Rails.application.routes.draw do
  get '/line/:line_index' => 'line#get'
end
